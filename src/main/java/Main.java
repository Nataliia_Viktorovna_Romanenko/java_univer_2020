import com.fasterxml.jackson.core.JsonProcessingException;
import model.Person;
import serialize.JsonSerializer;
import serialize.Serializer;
import serialize.TxtPersonSerializer;
import serialize.XmlSerializer;

import java.time.LocalDate;

public class Main {
    public static void main(String[] args) {
        Person p = new Person("Jan");
        p.setDateOfBirth(LocalDate.now().minusYears(25));
        Serializer<Person> ser = new JsonSerializer<>(Person.class);
        System.out.println(ser.toString(p));

        ser = new XmlSerializer<>(Person.class);
        System.out.println(ser.toString(p));

        ser = new TxtPersonSerializer(Person.class);
        System.out.println(ser.fromString("Name Jan || BirthDay 1995-10-04."));
    }
}
